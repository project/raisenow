<?php

namespace Drupal\raisenow_webform\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\raisenow\RaiseNowConfigEntity;
use Drupal\webform\Element\WebformAjaxElementTrait;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'form_raisenow_widget' element.
 *
 * @WebformElement(
 *   id = "form_raisenow_widget",
 *   label = @Translation("RaiseNow widget"),
 *   description = @Translation("Display a RaiseNow widget."),
 *   category = @Translation("RaiseNow"),
 *   composite = TRUE,
 * )
 */
class RaiseNowWidget extends WebformElementBase {

  use WebformAjaxElementTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    $properties = [
      // Color settings.
      'entity_type' => '',
      'entity_id' => '',
    ] + parent::defineDefaultProperties();
    unset(
      $properties['format_items'],
      $properties['format_items_html'],
      $properties['format_items_text']
    );
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entityTypes = RaiseNowConfigEntity::getEntityTypes();
    $entityTypeOptions = [
      '' => $this->t('- None -'),
    ];
    foreach($entityTypes as $entityTypeKey => $entityType) {
      $entityTypeOptions[$entityTypeKey] = $entityType->getLabel();
    }

    $element_properties = $form_state->get('element_properties');

    if ($form_state->isRebuilding()) {
      // Get values from user input because
      // $form_state->getValue() does not always contain every input's value.
      $user_input = $form_state->getUserInput()['properties'];
      $entity_type = (!empty($user_input['entity_type'])) ? $user_input['entity_type'] : '';
      //$entity_id = (!empty($user_input['raisenow_wrapper']['entity_id'])) ? $user_input['raisenow_wrapper']['entity_id'] : '';
    }
    else {
      $entity_type = $element_properties['entity_type'];
      //$entity_id = $element_properties['entity_id'];
    }

    $form['raisenow_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('RaiseNow widget'),
    ];

    $form['raisenow_wrapper']['entity_type'] = [
      '#title' => $this->t('Entity type'),
      '#type' => 'select',
      '#options' => $entityTypeOptions,
    ];

    $entityOptions = [
      '' => $this->t('- None -'),
    ];
    if (!empty($entity_type)) {
      $entities = RaiseNowConfigEntity::getEntities($entity_type);
      foreach ($entities[$entity_type] as $entityKey => $entity) {
        $entityOptions[$entityKey] = $entity->label();
      }
    }
    $form['raisenow_wrapper']['entity_id'] = [
      '#title' => $this->t('Entity ID'),
      '#type' => 'select',
      '#options' => $entityOptions,
    ];

    $ajax_id = 'webform-raisenow_widget';
    $this->buildAjaxElementWrapper($ajax_id, $form['raisenow_wrapper']);
    $this->buildAjaxElementUpdate($ajax_id, $form['raisenow_wrapper']);
    $this->buildAjaxElementTrigger($ajax_id, $form['raisenow_wrapper']['entity_type']);

    return $form;
  }

}
