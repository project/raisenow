<?php

namespace Drupal\raisenow_tamaro\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\raisenow_tamaro\SettingsParser;

/**
 * Class TamaroWidgetForm.
 */
class TamaroWidgetForm extends EntityForm {

  /** @var SettingsParser */
  protected $settingsParser;

  /**
   * Form constructor
   */
  public function __construct() {
    $this->settingsParser = new SettingsParser();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\raisenow_tamaro\Entity\TamaroWidget $tamaro_widget */
    $tamaro_widget = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $tamaro_widget->label(),
      '#description' => $this->t("Label for the Tamaro widget."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $tamaro_widget->id(),
      '#machine_name' => [
        'exists' => '\Drupal\raisenow_tamaro\Entity\TamaroWidget::load',
      ],
      '#disabled' => !$tamaro_widget->isNew(),
    ];

    $form['raisenow_config'] = $this->settingsParser->getFormApiElements($tamaro_widget->getRawConfig());
    $form['raisenow_config']['widget_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Widget URL'),
      '#default_value' => isset($tamaro_widget->getRawConfig()['widget_url']) ? $tamaro_widget->getRawConfig()['widget_url'] : '',
      '#weight' => -10,
      '#required' => TRUE,
    ];
    $form['raisenow_config']['#type'] = 'container';
    $form['raisenow_config']['#tree'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $tamaro_widget = $this->entity;
    $status = $tamaro_widget->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Tamaro widget.', [
          '%label' => $tamaro_widget->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Tamaro widget.', [
          '%label' => $tamaro_widget->label(),
        ]));
    }
    $form_state->setRedirectUrl($tamaro_widget->toUrl('collection'));
  }

  /**
   * @inheritDoc
   */
  public function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    $entity->set('raisenow_config', \GuzzleHttp\json_encode($form_state->getValue('raisenow_config')));
  }

}
