<?php

namespace Drupal\raisenow_tamaro\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\raisenow\RaiseNowConfigEntityInterface;

/**
 * Provides an interface for defining Tamaro widget entities.
 */
interface TamaroWidgetInterface extends ConfigEntityInterface {

}
