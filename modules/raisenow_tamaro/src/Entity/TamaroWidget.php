<?php

namespace Drupal\raisenow_tamaro\Entity;

use Drupal\raisenow\RaiseNowConfigEntity;
use Drupal\raisenow_tamaro\TamaroConfig;

/**
 * Defines the Tamaro widget entity.
 *
 * @ConfigEntityType(
 *   id = "tamaro_widget",
 *   label = @Translation("Tamaro widget"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\raisenow_tamaro\TamaroWidgetListBuilder",
 *     "form" = {
 *       "add" = "Drupal\raisenow_tamaro\Form\TamaroWidgetForm",
 *       "edit" = "Drupal\raisenow_tamaro\Form\TamaroWidgetForm",
 *       "delete" = "Drupal\raisenow_tamaro\Form\TamaroWidgetDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\raisenow_tamaro\TamaroWidgetHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "tamaro_widget",
 *   config_export = {
 *     "id",
 *     "label",
 *     "raisenow_config",
 *   },
 *   admin_permission = "administer raisenow settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/raisenow/tamaro_widget/{tamaro_widget}",
 *     "add-form" = "/admin/content/raisenow/tamaro_widget/add",
 *     "edit-form" = "/admin/content/raisenow/tamaro_widget/{tamaro_widget}/edit",
 *     "delete-form" = "/admin/content/raisenow/tamaro_widget/{tamaro_widget}/delete",
 *     "collection" = "/admin/content/raisenow/tamaro_widget"
 *   }
 * )
 */
class TamaroWidget extends RaiseNowConfigEntity implements TamaroWidgetInterface {

  /**
   * The Tamaro widget ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Tamaro widget label.
   *
   * @var string
   */
  protected $label;

  /**
   * The config
   *
   * @var string
   */
  protected $raisenow_config;

  /**
   * @var \Drupal\raisenow_tamaro\TamaroConfig
   */
  protected $config;

  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    $this->config = new TamaroConfig($this->getRawConfig());
  }

  /**
   * @return \Drupal\raisenow_tamaro\TamaroConfig
   */
  public function getConfig(): TamaroConfig {
    return $this->config;
  }

  /**
   * @inheritDoc
   */
  public function getRawConfig() {
    try {
      return \GuzzleHttp\json_decode($this->raisenow_config, TRUE);
    }
    catch (\Throwable $ex) {
      return [];
    }
  }

  /**
   * @inheritDoc
   */
  public function getWidgetUrl() {
    $config = $this->getRawConfig();
    return !empty($config['widget_url']) ? $config['widget_url'] : '';
  }

  /**
   * @inheritDoc
   */
  public function getWidgetRenderArray(array $element) {
    $options = $this->config->getConfigArray();
    $options = array_merge_recursive($options, $this->raiseNowOptions);

    $element['tamaro_widget'] = [
      '#markup' => "<div class='rnw-widget-container'></div>",
    ];


    $options["amounts"] = [
      [
        "if" => "currency() == chf",
        "then" => [
          [
            "if" => "paymentType() == onetime",
            "then" => [
              5,
              10,
              30,
              120
            ]
          ],
          [
            "if" => "paymentType() == recurring",
            "then" => [
              [
                "if" => "recurringInterval() == monthly",
                "then" => [
                  15,
                  20,
                  30,
                  120
                ]
              ],
              [
                "if" => "recurringInterval() == quarterly",
                "then" => [
                  45,
                  60,
                  90,
                  360
                ]
              ],
              [
                "if" => "recurringInterval() == semestral",
                "then" => [
                  90,
                  120,
                  180,
                  720
                ]
              ],
              [
                "if" => "recurringInterval() == yearly",
                "then" => [
                  180,
                  240,
                  360,
                  1440
                ]
              ]
            ]
          ]
        ]
      ],
      [
        "if" => "currency() == usd",
        "then" => [
          15,
          20,
          30,
          60
        ]
      ],
      [
        "if" => "currency() == eur",
        "then" => [
          5,
          15,
          30,
          90
        ]
      ]
    ];

    \Drupal::moduleHandler()->alter('raisenow_widget_options', $options, $element);

    $element['#attached']['library'][] = 'raisenow/raisenow.widget_' . $element['#entity_type'] . '_' . $element['#entity_id'];
    $element['#attached']['library'][] = 'raisenow_tamaro/raisenow_tamaro';
    $element['#attached']['drupalSettings']['raiseNowTamaro'] = [
      'options' => $options,
    ];
    return $element;
  }

}
