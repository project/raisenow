<?php

namespace Drupal\raisenow_tamaro;

use Drupal\Core\Serialization\Yaml;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Settings parser for RaiseNow Configuration Options
 *
 * https://support.raisenow.com/hc/en-us/articles/360018786778-Adding-conditions-in-your-configuration
 */
class SettingsParser {

  use StringTranslationTrait;

  /** @var array  */
  protected $csv_settings;

  /** @var array  */
  protected $groups;

  /** @var array  */
  protected $form_api_settings;

  /**
   * SettingsParser constructor
   */
  public function __construct() {
    /** @var \Drupal\Core\Extension\ExtensionPathResolver $service */
    $service = \Drupal::service('extension.path.resolver');
    $module_path = $service->getPath('module', 'raisenow_tamaro');
    $csv_filename = $module_path . '/settings/RaiseNowConfigurationOptions.txt';
    $csv_file = file($csv_filename);
    $this->csv_settings = [];
    $headers = array_shift($csv_file);
    $headers = str_getcsv($headers, "\t");
    foreach ($csv_file as $line) {
      $line = str_getcsv($line, "\t");
      $key = $line[0];
      $config = [];
      foreach($headers as $ctr => $header) {
        $header = strtolower($header);
        $config[$header] = $line[$ctr];
      }
      $config['yaml_key'] = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $key));
      $config['label'] = ucwords(str_replace('_', ' ', $config['yaml_key']));
      $this->csv_settings[$key] = $config;
    }
    $yaml_filename = $module_path . '/settings/form_groups.yml';
    $this->groups = Yaml::decode(file_get_contents($yaml_filename));
  }

  /**
   * @return array|null
   */
  public function getGroups() {
    return $this->groups;
  }

  /**
   * @return mixed|null
   */
  public function getRaiseNowProperties() {
    return $this->csv_settings;
  }

  /**
   * @param $default_values
   *
   * @return array
   */
  public function getFormApiElements($default_values) {
    $form = [];
    foreach($this->csv_settings as $key => $csv_setting) {
      if ($csv_setting['group'] == 'hidden') {
        continue;
      }
      if (empty($form[$csv_setting['group']])) {
        $title = $csv_setting['group'];
        $title = str_replace('_', ' ', $title);
        $title = ucwords($title);
        $form[$csv_setting['group']] = [
          '#type' => 'details',
          '#title' => $this->t($title),
          '#open' => FALSE,
          '#true' => TRUE,
        ];
      }
      $type = $this->getFormApiType($csv_setting['type'], $csv_setting['formtype']);
      if (empty($type)) {
        continue;
      }
      $form[$csv_setting['group']][$key] = [
        '#type' => $type,
        '#title' => $this->t($csv_setting['label']),
        '#description' => $csv_setting['description'],
      ];
      if ($csv_setting['default'] !== '') {
        $form[$csv_setting['group']][$key]['#default_value'] = $csv_setting['default'];
      }
      if (substr($type, -2, 2) == '[]') {
        $form[$csv_setting['group']][$key]['#type'] = substr($type, 0, -2);
        $form[$csv_setting['group']][$key]['#multiple'] = TRUE;
        if (isset($csv_setting['default'])) {
          $default = explode(',', $csv_setting['default']);
          $form[$csv_setting['group']][$key]['#default_value'] = $default;
        }
      }
      $options = $this->getFormApiOptions($csv_setting);
      if (!empty($options)) {
        $form[$csv_setting['group']][$key]['#options'] = $options;
      }
      if (isset($default_values[$csv_setting['group']][$key])) {
        $form[$csv_setting['group']][$key]['#default_value'] = $default_values[$csv_setting['group']][$key];
      }
    }
    return $form;
  }

  /**
   * @param $raiseNowType
   * @param $formType
   *
   * @return bool|mixed|string
   */
  protected function getFormApiType($raiseNowType, $formType) {
    if (!empty($formType)) {
      return $formType;
    }
    $types = [
      'boolean' => 'checkbox',
      'string' => 'textfield',
      'number' => 'number',
      'string[]' => 'textarea',
    ];
    return $types[$raiseNowType] ?? NULL;
  }

  /**
   * @param $csv_line
   *
   * @return array|null
   */
  protected function getFormApiOptions($csv_line) {
    $options = NULL;
    if (!empty($csv_line['options'])) {
      if (substr($csv_line['options'], 0, 9) == 'callback:') {
        try {
          $callback = substr($csv_line['options'], 9);
          $options = call_user_func($callback);
        }
        catch (\Throwable $ex) {
          $a=1;
        }
      }
      else {
        $options = explode(',', $csv_line['options']);
        $options = array_combine($options, $options);
      }
    }
    return $options;
  }
}
