<?php

namespace Drupal\raisenow_tamaro;

use Drupal\raisenow\ConfigJsonSerializer;

class TamaroConfig implements \JsonSerializable {

  use ConfigJsonSerializer;

  public const OVERLAY_MODE_INNER = 'inner';
  public const OVERLAY_MODE_OUTER = 'outer';

  public const PAYMENT_FLOW_EPP = 'epp';
  public const PAYMENT_FLOW_EPMS = 'epms';

  public const LAYOUT_LIST = 'list';
  public const LAYOUT_STEP = 'step';

  protected $configSources;

  /**
   * @param array $config
   */
  public function __construct(array $config) {
    $settingsParser = new SettingsParser();
    $this->configSources = $settingsParser->getRaiseNowProperties();
    foreach($config as $group) {
      if (is_array($group)) {
        foreach ($group as $key => $value) {
          try {
            if (isset($value) && $value !== '') {
              $this->{$key} = $value;
            }
          } catch (\Throwable $ex) {
            $a = 1;
          }
        }
      }
    }
  }

  /**
   * @var boolean
   */
  protected $debug;

  /**
   * @var boolean
   */
  protected $debugErrorMessages;

  /**
   * @var boolean
   */
  protected $debugSlots;

  /**
   * ?
   * @var string
   */
  protected $overlayMode;

  /**
   * @var array
   */
  protected $epikConfig;

  /**
   * @var boolean
   */
  protected $testMode;

  /**
   * @var boolean
   */
  protected $returnParameters;

  /**
   * @var string
   */
  protected $language;

  /**
   * ?
   * @var string
   */
  protected $flow;

  /**
   * ?
   * @var string
   */
  protected $layout;

  /**
   * @var string[]
   */
  protected $paymentWidgetBlocks;

  /**
   * ?
   * @var string[]
   */
  protected $subscriptionWidgetBlocks;

  /**
   * @var array
   */
  protected $translations;

  /**
   * @var string
   */
  protected $paymentFormPrefill;

  /**
   * @var string
   */
  protected $purposes;

  /**
   * @var string
   */
  protected $defaultPurpose;

  /**
   * @var boolean
   */
  protected $autoselectPurpose;

  /**
   * ?
   * @var string
   */
  protected $purposeDetails;

  /**
   * @var string
   */
  protected $paymentMethods;

  /**
   * @var string
   */
  protected $defaultPaymentMethod;

  /**
   * @var boolean
   */
  protected $autoselectPaymentMethod;

  /**
   * @var string
   */
  protected $paymentTypes;

  /**
   * @var string
   */
  protected $defaultPaymentType;

  /**
   * @var boolean
   */
  protected $autoselectPaymentType;

  /**
   * @var string
   */
  protected $recurringIntervals;

  /**
   * @var string
   */
  protected $defaultRecurringInterval;

  /**
   * @var string
   */
  protected $autoselectRecurringInterval;

  /**
   * @var string[]
   */
  protected $currencies;

  /**
   * @var string
   */
  protected $defaultCurrency;

  /**
   * @var boolean
   */
  protected $autoselectCurrency;

  /**
   * @var array
   */
  protected $amounts;

  /**
   * @var string
   */
  protected $defaultAmount;

  /**
   * @var boolean
   */
  protected $autoselectAmount;

  /**
   * @var boolean
   */
  protected $allowCustomAmount;

  /**
   * @var string
   */
  protected $minimumCustomAmount;

  /**
   * ?
   * @var boolean
   */
  protected $useCreditCardIframe;

  /**
   * ?
   * @var boolean
   */
  protected $showPaymentIframe;

  /**
   * @var string[]
   */
  protected $salutations;

  /**
   * @var string[]
   */
  protected $countries;

  /**
   * @var array
   */
  protected $paymentValidations;

  /**
   * ?
   * @var array
   */
  protected $customerUpdateValidations;

  /**
   * @var array
   */
  protected $amountSubunits;

  /**
   * @var array
   */
  protected $uiBreakpoints;

  /**
   * ?
   * @var integer
   */
  protected $uiTransitionTimeout;

  /**
   * ?
   * @var integer
   */
  protected $uiScrollOffset;

  /**
   * ?
   * @var integer
   */
  protected $uiScrollDuration;

  /**
   * ?
   * @var array
   */
  protected $slots;

  /**
   * ?
   * @var string
   */
  protected $recaptchaKey;

  /**
   * ?
   * @var string[]
   */
  protected $faqEntries;

  /**
   * @var boolean
   */
  protected $showStoredCustomerBirthdate;

  /**
   * @var boolean
   */
  protected $showStoredCustomerEmailPermission;

  /**
   * @var boolean
   */
  protected $showStoredCustomerMessage;

  /**
   * @var boolean
   */
  protected $showStoredCustomerDonationReceipt;

  /**
   * @var boolean
   */
  protected $showStoredCustomerStreetNumber;

  /**
   * @var boolean
   */
  protected $showStoredCustomerStreet2;

  /**
   * @var boolean
   */
  protected $showStoredCustomerPobox;

  /**
   * @var boolean
   */
  protected $showTestModeBar;

  /**
   * @return bool
   */
  public function getDebug(): bool {
    return $this->debug;
  }

  /**
   * @param bool $debug
   *
   * @return TamaroConfig
   */
  public function setDebug(bool $debug): TamaroConfig {
    $this->debug = $debug;
    return $this;
  }

  /**
   * @return bool
   */
  public function getDebugErrorMessages(): bool {
    return $this->debugErrorMessages;
  }

  /**
   * @param bool $debugErrorMessages
   *
   * @return TamaroConfig
   */
  public function setDebugErrorMessages(bool $debugErrorMessages): TamaroConfig {
    $this->debugErrorMessages = $debugErrorMessages;
    return $this;
  }

  /**
   * @return bool
   */
  public function getDebugSlots(): bool {
    return $this->debugSlots;
  }

  /**
   * @param bool $debugSlots
   *
   * @return TamaroConfig
   */
  public function setDebugSlots(bool $debugSlots): TamaroConfig {
    $this->debugSlots = $debugSlots;
    return $this;
  }

  /**
   * @return string
   */
  public function getOverlayMode(): string {
    return $this->overlayMode;
  }

  /**
   * @param string $overlayMode
   *
   * @return TamaroConfig
   */
  public function setOverlayMode(string $overlayMode): TamaroConfig {
    $this->overlayMode = $overlayMode;
    return $this;
  }

  /**
   * @return array
   */
  public function getEpikConfig(): array {
    return $this->epikConfig;
  }

  /**
   * @param array $epikConfig
   *
   * @return TamaroConfig
   */
  public function setEpikConfig(array $epikConfig): TamaroConfig {
    $this->epikConfig = $epikConfig;
    return $this;
  }

  /**
   * @return bool
   */
  public function getTestMode(): bool {
    return $this->testMode;
  }

  /**
   * @param bool $testMode
   *
   * @return TamaroConfig
   */
  public function setTestMode(bool $testMode): TamaroConfig {
    $this->testMode = $testMode;
    return $this;
  }

  /**
   * @return bool
   */
  public function getReturnParameters(): bool {
    return $this->returnParameters;
  }

  /**
   * @param bool $returnParameters
   *
   * @return TamaroConfig
   */
  public function setReturnParameters(bool $returnParameters): TamaroConfig {
    $this->returnParameters = $returnParameters;
    return $this;
  }

  /**
   * @return string
   */
  public function getLanguage(): string {
    return $this->language;
  }

  /**
   * @param string $language
   *
   * @return TamaroConfig
   */
  public function setLanguage(string $language): TamaroConfig {
    $this->language = $language;
    return $this;
  }

  /**
   * @return string
   */
  public function getFlow(): string {
    return $this->flow;
  }

  /**
   * @param string $flow
   *
   * @return TamaroConfig
   */
  public function setFlow(string $flow): TamaroConfig {
    $this->flow = $flow;
    return $this;
  }

  /**
   * @return string
   */
  public function getLayout(): string {
    return $this->layout;
  }

  /**
   * @param string $layout
   *
   * @return TamaroConfig
   */
  public function setLayout(string $layout): TamaroConfig {
    $this->layout = $layout;
    return $this;
  }

  /**
   * @return string[]
   */
  public function getPaymentWidgetBlocks(): array {
    return $this->paymentWidgetBlocks;
  }

  /**
   * @param string[] $paymentWidgetBlocks
   *
   * @return TamaroConfig
   */
  public function setPaymentWidgetBlocks(array $paymentWidgetBlocks): TamaroConfig {
    $this->paymentWidgetBlocks = $paymentWidgetBlocks;
    return $this;
  }

  /**
   * @return string[]
   */
  public function getSubscriptionWidgetBlocks(): array {
    return $this->subscriptionWidgetBlocks;
  }

  /**
   * @param string[] $subscriptionWidgetBlocks
   *
   * @return TamaroConfig
   */
  public function setSubscriptionWidgetBlocks(array $subscriptionWidgetBlocks): TamaroConfig {
    $this->subscriptionWidgetBlocks = $subscriptionWidgetBlocks;
    return $this;
  }

  /**
   * @return array
   */
  public function getTranslations(): array {
    return $this->translations;
  }

  /**
   * @param array $translations
   *
   * @return TamaroConfig
   */
  public function setTranslations(array $translations): TamaroConfig {
    $this->translations = $translations;
    return $this;
  }

  /**
   * @return string
   */
  public function getPaymentFormPrefill(): string {
    return $this->paymentFormPrefill;
  }

  /**
   * @param string $paymentFormPrefill
   *
   * @return TamaroConfig
   */
  public function setPaymentFormPrefill(string $paymentFormPrefill): TamaroConfig {
    $this->paymentFormPrefill = $paymentFormPrefill;
    return $this;
  }

  /**
   * @return string
   */
  public function getPurposes(): string {
    return $this->purposes;
  }

  /**
   * @param string $purposes
   *
   * @return TamaroConfig
   */
  public function setPurposes(string $purposes): TamaroConfig {
    $this->purposes = $purposes;
    return $this;
  }

  /**
   * @return string
   */
  public function getDefaultPurpose(): string {
    return $this->defaultPurpose;
  }

  /**
   * @param string $defaultPurpose
   *
   * @return TamaroConfig
   */
  public function setDefaultPurpose(string $defaultPurpose): TamaroConfig {
    $this->defaultPurpose = $defaultPurpose;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAutoselectPurpose(): bool {
    return $this->autoselectPurpose;
  }

  /**
   * @param bool $autoselectPurpose
   *
   * @return TamaroConfig
   */
  public function setAutoselectPurpose(bool $autoselectPurpose): TamaroConfig {
    $this->autoselectPurpose = $autoselectPurpose;
    return $this;
  }

  /**
   * @return string
   */
  public function getPurposeDetails(): string {
    return $this->purposeDetails;
  }

  /**
   * @param string $purposeDetails
   *
   * @return TamaroConfig
   */
  public function setPurposeDetails(string $purposeDetails): TamaroConfig {
    $this->purposeDetails = $purposeDetails;
    return $this;
  }

  /**
   * @return string
   */
  public function getPaymentMethods(): string {
    return $this->paymentMethods;
  }

  /**
   * @param string $paymentMethods
   *
   * @return TamaroConfig
   */
  public function setPaymentMethods(string $paymentMethods): TamaroConfig {
    $this->paymentMethods = $paymentMethods;
    return $this;
  }

  /**
   * @return string
   */
  public function getDefaultPaymentMethod(): string {
    return $this->defaultPaymentMethod;
  }

  /**
   * @param string $defaultPaymentMethod
   *
   * @return TamaroConfig
   */
  public function setDefaultPaymentMethod(string $defaultPaymentMethod): TamaroConfig {
    $this->defaultPaymentMethod = $defaultPaymentMethod;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAutoselectPaymentMethod(): bool {
    return $this->autoselectPaymentMethod;
  }

  /**
   * @param bool $autoselectPaymentMethod
   *
   * @return TamaroConfig
   */
  public function setAutoselectPaymentMethod(bool $autoselectPaymentMethod): TamaroConfig {
    $this->autoselectPaymentMethod = $autoselectPaymentMethod;
    return $this;
  }

  /**
   * @return string
   */
  public function getPaymentTypes(): string {
    return $this->paymentTypes;
  }

  /**
   * @param string $paymentTypes
   *
   * @return TamaroConfig
   */
  public function setPaymentTypes(string $paymentTypes): TamaroConfig {
    $this->paymentTypes = $paymentTypes;
    return $this;
  }

  /**
   * @return string
   */
  public function getDefaultPaymentType(): string {
    return $this->defaultPaymentType;
  }

  /**
   * @param string $defaultPaymentType
   *
   * @return TamaroConfig
   */
  public function setDefaultPaymentType(string $defaultPaymentType): TamaroConfig {
    $this->defaultPaymentType = $defaultPaymentType;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAutoselectPaymentType(): bool {
    return $this->autoselectPaymentType;
  }

  /**
   * @param bool $autoselectPaymentType
   *
   * @return TamaroConfig
   */
  public function setAutoselectPaymentType(bool $autoselectPaymentType): TamaroConfig {
    $this->autoselectPaymentType = $autoselectPaymentType;
    return $this;
  }

  /**
   * @return string
   */
  public function getRecurringIntervals(): string {
    return $this->recurringIntervals;
  }

  /**
   * @param string $recurringIntervals
   *
   * @return TamaroConfig
   */
  public function setRecurringIntervals(string $recurringIntervals): TamaroConfig {
    $this->recurringIntervals = $recurringIntervals;
    return $this;
  }

  /**
   * @return string
   */
  public function getDefaultRecurringInterval(): string {
    return $this->defaultRecurringInterval;
  }

  /**
   * @param string $defaultRecurringInterval
   *
   * @return TamaroConfig
   */
  public function setDefaultRecurringInterval(string $defaultRecurringInterval): TamaroConfig {
    $this->defaultRecurringInterval = $defaultRecurringInterval;
    return $this;
  }

  /**
   * @return string
   */
  public function getAutoselectRecurringInterval(): string {
    return $this->autoselectRecurringInterval;
  }

  /**
   * @param string $autoselectRecurringInterval
   *
   * @return TamaroConfig
   */
  public function setAutoselectRecurringInterval(string $autoselectRecurringInterval): TamaroConfig {
    $this->autoselectRecurringInterval = $autoselectRecurringInterval;
    return $this;
  }

  /**
   * @return string[]
   */
  public function getCurrencies(): array {
    return $this->currencies;
  }

  /**
   * @param string[] $currencies
   *
   * @return TamaroConfig
   */
  public function setCurrencies(array $currencies): TamaroConfig {
    $this->currencies = $currencies;
    return $this;
  }

  /**
   * @return string
   */
  public function getDefaultCurrency(): string {
    return $this->defaultCurrency;
  }

  /**
   * @param string $defaultCurrency
   *
   * @return TamaroConfig
   */
  public function setDefaultCurrency(string $defaultCurrency): TamaroConfig {
    $this->defaultCurrency = $defaultCurrency;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAutoselectCurrency(): bool {
    return $this->autoselectCurrency;
  }

  /**
   * @param bool $autoselectCurrency
   *
   * @return TamaroConfig
   */
  public function setAutoselectCurrency(bool $autoselectCurrency): TamaroConfig {
    $this->autoselectCurrency = $autoselectCurrency;
    return $this;
  }

  /**
   * @return array
   */
  public function getAmounts(): array {
    return $this->amounts;
  }

  /**
   * @param array $amounts
   *
   * @return TamaroConfig
   */
  public function setAmounts(array $amounts): TamaroConfig {
    $this->amounts = $amounts;
    return $this;
  }

  /**
   * @return string
   */
  public function getDefaultAmount(): string {
    return $this->defaultAmount;
  }

  /**
   * @param string $defaultAmount
   *
   * @return TamaroConfig
   */
  public function setDefaultAmount(string $defaultAmount): TamaroConfig {
    $this->defaultAmount = $defaultAmount;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAutoselectAmount(): bool {
    return $this->autoselectAmount;
  }

  /**
   * @param bool $autoselectAmount
   *
   * @return TamaroConfig
   */
  public function setAutoselectAmount(bool $autoselectAmount): TamaroConfig {
    $this->autoselectAmount = $autoselectAmount;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAllowCustomAmount(): bool {
    return $this->allowCustomAmount;
  }

  /**
   * @param bool $allowCustomAmount
   *
   * @return TamaroConfig
   */
  public function setAllowCustomAmount(bool $allowCustomAmount): TamaroConfig {
    $this->allowCustomAmount = $allowCustomAmount;
    return $this;
  }

  /**
   * @return string
   */
  public function getMinimumCustomAmount(): string {
    return $this->minimumCustomAmount;
  }

  /**
   * @param string $minimumCustomAmount
   *
   * @return TamaroConfig
   */
  public function setMinimumCustomAmount(string $minimumCustomAmount): TamaroConfig {
    $this->minimumCustomAmount = $minimumCustomAmount;
    return $this;
  }

  /**
   * @return bool
   */
  public function getUseCreditCardIframe(): bool {
    return $this->useCreditCardIframe;
  }

  /**
   * @param bool $useCreditCardIframe
   *
   * @return TamaroConfig
   */
  public function setUseCreditCardIframe(bool $useCreditCardIframe): TamaroConfig {
    $this->useCreditCardIframe = $useCreditCardIframe;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowPaymentIframe(): bool {
    return $this->showPaymentIframe;
  }

  /**
   * @param bool $showPaymentIframe
   *
   * @return TamaroConfig
   */
  public function setShowPaymentIframe(bool $showPaymentIframe): TamaroConfig {
    $this->showPaymentIframe = $showPaymentIframe;
    return $this;
  }

  /**
   * @return string[]
   */
  public function getSalutations(): array {
    return $this->salutations;
  }

  /**
   * @param string[] $salutations
   *
   * @return TamaroConfig
   */
  public function setSalutations(array $salutations): TamaroConfig {
    $this->salutations = $salutations;
    return $this;
  }

  /**
   * @return string[]
   */
  public function getCountries(): array {
    return $this->countries;
  }

  /**
   * @param string[] $countries
   *
   * @return TamaroConfig
   */
  public function setCountries(array $countries): TamaroConfig {
    $this->countries = $countries;
    return $this;
  }

  /**
   * @return array
   */
  public function getPaymentValidations(): array {
    return $this->paymentValidations;
  }

  /**
   * @param array $paymentValidations
   *
   * @return TamaroConfig
   */
  public function setPaymentValidations(array $paymentValidations): TamaroConfig {
    $this->paymentValidations = $paymentValidations;
    return $this;
  }

  /**
   * @return array
   */
  public function getCustomerUpdateValidations(): array {
    return $this->customerUpdateValidations;
  }

  /**
   * @param array $customerUpdateValidations
   *
   * @return TamaroConfig
   */
  public function setCustomerUpdateValidations(array $customerUpdateValidations): TamaroConfig {
    $this->customerUpdateValidations = $customerUpdateValidations;
    return $this;
  }

  /**
   * @return array
   */
  public function getAmountSubunits(): array {
    return $this->amountSubunits;
  }

  /**
   * @param array $amountSubunits
   *
   * @return TamaroConfig
   */
  public function setAmountSubunits(array $amountSubunits): TamaroConfig {
    $this->amountSubunits = $amountSubunits;
    return $this;
  }

  /**
   * @return array
   */
  public function getUiBreakpoints(): array {
    return $this->uiBreakpoints;
  }

  /**
   * @param array $uiBreakpoints
   *
   * @return TamaroConfig
   */
  public function setUiBreakpoints(array $uiBreakpoints): TamaroConfig {
    $this->uiBreakpoints = $uiBreakpoints;
    return $this;
  }

  /**
   * @return int
   */
  public function getUiTransitionTimeout(): int {
    return $this->uiTransitionTimeout;
  }

  /**
   * @param int $uiTransitionTimeout
   *
   * @return TamaroConfig
   */
  public function setUiTransitionTimeout(int $uiTransitionTimeout): TamaroConfig {
    $this->uiTransitionTimeout = $uiTransitionTimeout;
    return $this;
  }

  /**
   * @return int
   */
  public function getUiScrollOffset(): int {
    return $this->uiScrollOffset;
  }

  /**
   * @param int $uiScrollOffset
   *
   * @return TamaroConfig
   */
  public function setUiScrollOffset(int $uiScrollOffset): TamaroConfig {
    $this->uiScrollOffset = $uiScrollOffset;
    return $this;
  }

  /**
   * @return int
   */
  public function getUiScrollDuration(): int {
    return $this->uiScrollDuration;
  }

  /**
   * @param int $uiScrollDuration
   *
   * @return TamaroConfig
   */
  public function setUiScrollDuration(int $uiScrollDuration): TamaroConfig {
    $this->uiScrollDuration = $uiScrollDuration;
    return $this;
  }

  /**
   * @return array
   */
  public function getSlots(): array {
    return $this->slots;
  }

  /**
   * @param array $slots
   *
   * @return TamaroConfig
   */
  public function setSlots(array $slots): TamaroConfig {
    $this->slots = $slots;
    return $this;
  }

  /**
   * @return string
   */
  public function getRecaptchaKey(): string {
    return $this->recaptchaKey;
  }

  /**
   * @param string $recaptchaKey
   *
   * @return TamaroConfig
   */
  public function setRecaptchaKey(string $recaptchaKey): TamaroConfig {
    $this->recaptchaKey = $recaptchaKey;
    return $this;
  }

  /**
   * @return string[]
   */
  public function getFaqEntries(): array {
    return $this->faqEntries;
  }

  /**
   * @param string[] $faqEntries
   *
   * @return TamaroConfig
   */
  public function setFaqEntries(array $faqEntries): TamaroConfig {
    $this->faqEntries = $faqEntries;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerBirthdate(): bool {
    return $this->showStoredCustomerBirthdate;
  }

  /**
   * @param bool $showStoredCustomerBirthdate
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerBirthdate(bool $showStoredCustomerBirthdate): TamaroConfig {
    $this->showStoredCustomerBirthdate = $showStoredCustomerBirthdate;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerEmailPermission(): bool {
    return $this->showStoredCustomerEmailPermission;
  }

  /**
   * @param bool $showStoredCustomerEmailPermission
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerEmailPermission(bool $showStoredCustomerEmailPermission): TamaroConfig {
    $this->showStoredCustomerEmailPermission = $showStoredCustomerEmailPermission;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerMessage(): bool {
    return $this->showStoredCustomerMessage;
  }

  /**
   * @param bool $showStoredCustomerMessage
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerMessage(bool $showStoredCustomerMessage): TamaroConfig {
    $this->showStoredCustomerMessage = $showStoredCustomerMessage;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerDonationReceipt(): bool {
    return $this->showStoredCustomerDonationReceipt;
  }

  /**
   * @param bool $showStoredCustomerDonationReceipt
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerDonationReceipt(bool $showStoredCustomerDonationReceipt): TamaroConfig {
    $this->showStoredCustomerDonationReceipt = $showStoredCustomerDonationReceipt;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerStreetNumber(): bool {
    return $this->showStoredCustomerStreetNumber;
  }

  /**
   * @param bool $showStoredCustomerStreetNumber
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerStreetNumber(bool $showStoredCustomerStreetNumber): TamaroConfig {
    $this->showStoredCustomerStreetNumber = $showStoredCustomerStreetNumber;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerStreet2(): bool {
    return $this->showStoredCustomerStreet2;
  }

  /**
   * @param bool $showStoredCustomerStreet2
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerStreet2(bool $showStoredCustomerStreet2): TamaroConfig {
    $this->showStoredCustomerStreet2 = $showStoredCustomerStreet2;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowStoredCustomerPobox(): bool {
    return $this->showStoredCustomerPobox;
  }

  /**
   * @param bool $showStoredCustomerPobox
   *
   * @return TamaroConfig
   */
  public function setShowStoredCustomerPobox(bool $showStoredCustomerPobox): TamaroConfig {
    $this->showStoredCustomerPobox = $showStoredCustomerPobox;
    return $this;
  }

  /**
   * @return bool
   */
  public function getShowTestModeBar(): bool {
    return $this->showTestModeBar;
  }

  /**
   * @param bool $showTestModeBar
   *
   * @return TamaroConfig
   */
  public function setShowTestModeBar(bool $showTestModeBar): TamaroConfig {
    $this->showTestModeBar = $showTestModeBar;
    return $this;
  }



}
