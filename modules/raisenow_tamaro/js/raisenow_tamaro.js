/**
 * @file
 * RaiseNow Tamaro JS.
 */

(function ($, window, Drupal, drupalSettings) {

  'use strict';

  var checkout_type = 'default';

  /**
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.raiseNowTamaro = {
    attach: function (context, settings) {
      var options = settings.raiseNowTamaro.options;
      window.rnw.tamaro.runWidget('.rnw-widget-container', options).then(function (api) {
        window.api = api;
      });
    }
  };

})(jQuery, window, Drupal, drupalSettings);
