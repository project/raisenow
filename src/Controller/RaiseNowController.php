<?php

namespace Drupal\raisenow\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class RaiseNowController.
 */
class RaiseNowController extends ControllerBase {

  /**
   * Systemmeny.
   *
   * @return string
   *   Return Hello string.
   */
  public function systemMenu() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: SystemMenu')
    ];
  }

}
