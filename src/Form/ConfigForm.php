<?php

namespace Drupal\raisenow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'raisenow.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('raisenow.config');
    $form['api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#description' => $this->t('The Api base URL. Be sure to include the trailing slash, e.g. https://api.raisenow.com/api/v2/'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_base_url'),
    ];
    $form['api_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API Key'),
      '#key_filters' => [
        'type_group' => 'authentication',
      ],
      '#maxlength' => 255,
      '#default_value' => $config->get('api_key'),
    ];
    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Username'),
      '#description' => $this->t('The Api Username'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_username'),
    ];
    $form['api_password'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API Password'),
      '#key_filters' => [
        'type_group' => 'authentication',
      ],
      '#maxlength' => 255,
      '#default_value' => $config->get('api_password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('raisenow.config')
      ->set('api_base_url', $form_state->getValue('api_base_url'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_username', $form_state->getValue('api_username'))
      ->set('api_password', $form_state->getValue('api_password'))
      ->save();
  }

}
