<?php

namespace Drupal\raisenow;

trait ConfigJsonSerializer {

  /**
   * @return array
   */
  public function getConfigArray() {
    $var = get_object_vars($this);
    foreach ($var as $key => &$value) {
      if ($key == 'configSources') {
        unset($var[$key]);
        continue;
      }
      if (is_object($value) && method_exists($value, 'getJsonData')) {
        $value = $value->getJsonData();
      }
      $dataType = $this->getDataType($key);
      switch ($dataType) {
        case 'boolean' :
          $value = $value == 1;
          break;
        case 'number':
          $value = (int) $value;
          break;
        case 'string[]':
          $new_value = NULL;
          if (is_string($value) && $value !== '') {
            $value = explode(PHP_EOL, $value);
          }
          if (is_array($value)) {
            foreach ($value as $value_key => $value_value) {
              if (!empty($value_value)) {
                $new_value[] = $value_value;
              }
            }
          }
          $value = $new_value;
          break;
        case 'ContentBlockConfig[]':
          $new_value = NULL;
          if (is_string($value) && $value !== '') {
            $value = explode(PHP_EOL, $value);
          }
          if (is_array($value)) {
            foreach ($value as $value_key => $value_value) {
              if (!empty($value_value)) {
                if ($value_value == 'payment_address') {
                  $value_value = [
                    'if' => 'showPaymentAddress()',
                    'then' => 'payment_address',
                  ];
                }
                $new_value[] = $value_value;
              }
            }
          }
          $value = $new_value;
          break;
        case 'PurposeConfig[]':
          if (is_string($value) && $value !== '') {
            $value = explode(PHP_EOL, $value);
            $new_value = [];
            $languages = RaiseNowLocalisation::supportedLanguages();
            foreach ($languages as $language_key => $language) {
              foreach($value as $purpose_key => $purpose_value) {
                $rn_purpose_key = 'p' . ($purpose_key + 1);
                if ($this->defaultPurpose == $purpose_value) {
                  $var['defaultPurpose'] = $rn_purpose_key;
                }
                $this->translations[$language_key]['purposes'][$rn_purpose_key] = $purpose_value;
                $new_value[$purpose_key] = $rn_purpose_key;
              }
            }
            $value = $new_value;
          }
          break;
        case 'ConfigCondition<PaymentCurrency>[]':
          $new_value = NULL;
          if (is_string($value) && $value !== '') {
            $value = explode(PHP_EOL, $value);
          }
          if (is_array($value)) {
            $currencies = RaiseNowLocalisation::supportedCurrencies();
            $languages = RaiseNowLocalisation::supportedLanguages();
            foreach ($value as $value_key => $value_value) {
              if (!empty($value_value)) {
                $new_value[] = $value_value;
                foreach ($languages as $language_key => $language) {
                  $this->translations[$language_key]['currencies'][$value_value] = $currencies[$value_value];
                }
              }
            }
          }
          $value = $new_value;
          break;
        case 'TranslatorMappedResource':
          $value = $this->translations;
          break;
        case 'defaultPurpose':
          // do nothing;
      }
      if (!isset($value) || is_null($value)) {
        unset($var[$key]);
      }
    }
    if (!empty($this->translations)) {
      $var['translations'] = $this->translations;
    }
    return $var;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return $this->getConfigArray();
  }

  /**
   * @param $name
   *
   * @return mixed|string
   */
  public function getDataType($name) {
    return isset($this->configSources[$name]) ? $this->configSources[$name]['type'] : '';
  }

}
