<?php


namespace Drupal\raisenow;


use Drupal\Core\Serialization\Yaml;

class RaiseNowLocalisation {

  /**
   * @return array
   */
  public static function supportedCurrencies() {
    $default = [
      'usd' => 'USD'
    ];
    return static::getYaml('currencies.yml', $default);
  }

  /**
   * @return array
   */
  public static function supportedLanguages() {
    $default = [
      'en' => 'en'
    ];
    return static::getYaml('languages.yml', $default);
  }

  /**
   * @param $filename
   * @param array $default
   *
   * @return array
   */
  protected static function getYaml($filename, array $default) {
    try {
      $default_settings = \Drupal::service('extension.list.module')->getPath('raisenow') . '/settings/' . $filename;
      $yaml = Yaml::decode(file_get_contents($default_settings));
      if (isset($yaml['supported'])) {
        $new_yaml = [];
        foreach($yaml['supported'] as $key => $value) {
          $new_yaml[strtolower($key)] = $value;
        }
        return $new_yaml;
      }
      else {
        return $default;
      }
    }
    catch (\Throwable $ex) {
      return $default;
    }
  }
}
