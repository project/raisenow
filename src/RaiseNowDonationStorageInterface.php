<?php

namespace Drupal\raisenow;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\raisenow\Entity\RaiseNowDonationInterface;

/**
 * Defines the storage handler class for RaiseNow donation entities.
 *
 * This extends the base storage class, adding required special handling for
 * RaiseNow donation entities.
 *
 * @ingroup raisenow
 */
interface RaiseNowDonationStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of RaiseNow donation revision IDs for a specific RaiseNow donation.
   *
   * @param \Drupal\raisenow\Entity\RaiseNowDonationInterface $entity
   *   The RaiseNow donation entity.
   *
   * @return int[]
   *   RaiseNow donation revision IDs (in ascending order).
   */
  public function revisionIds(RaiseNowDonationInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as RaiseNow donation author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   RaiseNow donation revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
