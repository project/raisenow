<?php

namespace Drupal\raisenow;

use function GuzzleHttp\json_decode;
use Drupal\key\Entity\Key;
use GuzzleHttp\Client;

/**
 * Class RaiseNowWebApiClient
 *
 * @package Drupal\raisenow
 */
class RaiseNowWebApiClient extends Client {

  /** @var string $rest_base_url */
  protected $rest_base_url;

  /** @var string $api_key */
  protected $api_key;

  /**
   * @inheritDoc
   */
  public function __construct(array $config = [], $api_key = NULL) {
    // load module config
    $module_config = \Drupal::config('raisenow.config');

    // base URL and API key
    $this->rest_base_url = $module_config->get('api_base_url');
    if (empty($api_key)) {
      $api_key = Key::load($module_config->get('api_key'));
      $this->api_key = $api_key->getKeyValue();
    }
    else {
      $this->api_key = $api_key;
    }

    // authorization header
    $api_username = $module_config->get('api_username');
    $password_key = Key::load($module_config->get('api_password'));
    $api_password = $password_key->getKeyValue();
    $config['auth'] = [
      $api_username,
      $api_password
    ];
    parent::__construct($config);
  }

  /**
   * @param $transaction_id
   *
   * @return mixed
   */
  public function getTransaction($transaction_id) {
    $rest_url = $this->rest_base_url . 'transactions/' . $transaction_id;
    $response = self::get(
      $rest_url,
      [
        'query' => ['api_key' => $this->api_key]
      ]
    );
    $result = (string) $response->getBody();
    return json_decode($result, TRUE);
  }

  /**
   * @return mixed
   */
  public static function getTransactionFromCurrentPage($api_key = NULL) {
    $response = &drupal_static('raisenow_payment_response');
    if (empty($response)) {
      // try to read from the URL parameter
      $url_params = \Drupal::request()->query->all();
      if (isset($url_params['epp_transaction_id'])) {
        $config = [];
        $client = new RaiseNowWebApiClient($config, $api_key);
        if (empty($response)) {
          $response = $client->getTransaction($url_params['epp_transaction_id']);
        }
      }
    }
    return $response;
  }

  /**
   * @param array $transaction
   *
   * @return bool
   */
  public static function isTransactionSuccessful(array $transaction) {
    if (empty($transaction) || empty($transaction['status'])) {
      return FALSE;
    }
    $status = $transaction['status'];
    $success_statuses = [
      'success',
      'final_success',
    ];
    $success = FALSE;
    foreach($status as $status_item) {
      if (in_array($status_item['statusName'], $success_statuses)) {
        $success = TRUE;
      }
    }
    return $success;
  }
}
