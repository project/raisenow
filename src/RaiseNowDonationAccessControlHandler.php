<?php

namespace Drupal\raisenow;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the RaiseNow donation entity.
 *
 * @see \Drupal\raisenow\Entity\RaiseNowDonation.
 */
class RaiseNowDonationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\raisenow\Entity\RaiseNowDonationInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished raisenow donation entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published raisenow donation entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit raisenow donation entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete raisenow donation entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add raisenow donation entities');
  }


}
