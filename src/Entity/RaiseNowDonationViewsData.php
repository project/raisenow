<?php

namespace Drupal\raisenow\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for RaiseNow donation entities.
 */
class RaiseNowDonationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
