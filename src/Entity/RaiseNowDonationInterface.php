<?php

namespace Drupal\raisenow\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining RaiseNow donation entities.
 *
 * @ingroup raisenow
 */
interface RaiseNowDonationInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the RaiseNow donation name.
   *
   * @return string
   *   Name of the RaiseNow donation.
   */
  public function getName();

  /**
   * Sets the RaiseNow donation name.
   *
   * @param string $name
   *   The RaiseNow donation name.
   *
   * @return \Drupal\raisenow\Entity\RaiseNowDonationInterface
   *   The called RaiseNow donation entity.
   */
  public function setName($name);

  /**
   * Gets the RaiseNow donation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the RaiseNow donation.
   */
  public function getCreatedTime();

  /**
   * Sets the RaiseNow donation creation timestamp.
   *
   * @param int $timestamp
   *   The RaiseNow donation creation timestamp.
   *
   * @return \Drupal\raisenow\Entity\RaiseNowDonationInterface
   *   The called RaiseNow donation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the RaiseNow donation revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the RaiseNow donation revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\raisenow\Entity\RaiseNowDonationInterface
   *   The called RaiseNow donation entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the RaiseNow donation revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the RaiseNow donation revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\raisenow\Entity\RaiseNowDonationInterface
   *   The called RaiseNow donation entity.
   */
  public function setRevisionUserId($uid);

}
