<?php

namespace Drupal\raisenow;

interface RaiseNowConfigEntityInterface {

  /**
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   */
  public static function getEntityTypes();

  /**
   * @param string $entity_type
   *
   * @return array
   */
  public static function getEntities($entity_type = NULL);

  /**
   * @return array|null
   */
  public function getRawConfig();

  /**
   * Get the widget URL
   *
   * @return string
   */
  public function getWidgetUrl();

  /**
   * @param array $raisenow_options
   *
   * @return $this
   */
  public function setRaiseNowOptions(array $raisenow_options);

  /**
   * @param array $element
   *
   * @return array
   */
  public function getWidgetRenderArray(array $element);

  /**
   * @return array
   */
  public static function getMappings();

  /**
   * @return array
   */
  public static function getWidgetMappings();

}
