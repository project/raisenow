<?php

namespace Drupal\raisenow;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Serialization\Yaml;

abstract class RaiseNowConfigEntity extends ConfigEntityBase implements RaiseNowConfigEntityInterface {

  /** @var array */
  protected $raiseNowOptions;

  /**
   * @inheritDoc
   */
  public function setRaiseNowOptions(array $raisenow_options) {
    $this->raiseNowOptions = $raisenow_options;
    return $this;
  }

  /**
   * @inheritDoc
   */
  public static function getEntityTypes() {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $entities = array_filter($entityTypeManager->getDefinitions(), function (EntityTypeInterface $entity_type) {
      return in_array('Drupal\raisenow\RaiseNowConfigEntityInterface', class_implements($entity_type->getOriginalClass()));
    });
    return $entities;
  }

  /**
   * @inheritDoc
   */
  public static function getEntities($entity_type = NULL) {
    $entityTypes = static::getEntityTypes();
    if (!empty($entity_type) && empty($entityTypes[$entity_type])) {
      return [];
    }
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    if (!empty($entity_type)) {
      $entityType = $entityTypes[$entity_type];
      $entityTypes = [
        $entity_type => $entityType,
      ];
    }
    $entites = [];
    foreach($entityTypes as $key => $entityType) {
      $entites[$key] = $entityTypeManager->getStorage($key)->loadMultiple();
    }
    return $entites;
  }

  /**
   * @inheritDoc
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // clear the plugin cache to re-scan the RaiseNowWidget entity libraries (dynamic JS paths)
    $cache_clear = TRUE;
    if ($update) {
      $widget_url = $this->getWidgetUrl();
      if (!empty($this->original)) {
        $original_url = $this->original->getWidgetUrl();
        $cache_clear = $widget_url != $original_url;
      }
    }
    if ($cache_clear) {
      // clear libraries cache
      \Drupal::service('library.discovery')->clearCachedDefinitions();
      // Flush asset file caches.
      \Drupal::service('asset.css.collection_optimizer')->deleteAll();
      \Drupal::service('asset.js.collection_optimizer')->deleteAll();
      drupal_flush_all_caches();
    }
  }

  /**
   * @inheritDoc
   */
  public static function getMappings() {
    $entity_mappings = [];
    $entity_types = static::getEntityTypes();
    foreach($entity_types as $entity_type_id => $entity_type) {
      $class = $entity_type->getClass();
      $entity_mappings[$entity_type_id] = call_user_func([$class, 'getWidgetMappings']);
    }
    return $entity_mappings;
  }

  /**
   * @inheritDoc
   */
  public static function getWidgetMappings() {
    $namespace = static::class;
    $module_name = explode('\\', $namespace)[1];
    /** @var \Drupal\Core\Extension\ExtensionPathResolver $service */
    $service = \Drupal::service('extension.path.resolver');
    $module_path = $service->getPath('module', $module_name);
    $yaml_filename = $module_path . '/settings/data_mappings.yml';
    $data_mappings = Yaml::decode(file_get_contents($yaml_filename));
    return $data_mappings;
  }

}
