<?php

namespace Drupal\raisenow\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'raisenow_widget_item' field type.
 *
 * @FieldType(
 *   id = "raisenow_widget_item",
 *   label = @Translation("RaiseNow widget"),
 *   category = @Translation("RaiseNow"),
 *   default_widget = "raisenow_select_widget",
 *   default_formatter = "raisenow_widget_formatter"
 * )
 *
 */
class RaiseNowWidgetItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'entity_type' => '',
      'entity_id' => '',
    ];
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $entity_type = $this->get('entity_type')->getValue();
    $entity_id = $this->get('entity_id')->getValue();
    $empty = ($entity_type === NULL || $entity_type === '') && ($entity_id === NULL || $entity_type == '');
    return $empty;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['entity_type'] = DataDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setRequired(TRUE);
    $properties['entity_id'] = DataDefinition::create('string')
      ->setLabel(t('Entity ID'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'entity_type' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'RaiseNow entity type',
        'length' => 255,
      ],
      'entity_id' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'RaiseNow entity ID',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      'indexes' => [
        'entity_type' => ['entity_type'],
        'entity_id' => ['entity_id'],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['entity_type'] = $random->word(mt_rand(1, 50));
    $values['entity_id'] = $random->word(mt_rand(1, 50));
    return $values;
  }

}
