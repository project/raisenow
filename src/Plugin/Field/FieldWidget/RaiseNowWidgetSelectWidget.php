<?php

namespace Drupal\raisenow\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\raisenow\RaiseNow;
use Drupal\raisenow\RaiseNowConfigEntity;

/**
 * Defines the 'raisenow_select_widget' field widget.
 *
 * @FieldWidget(
 *   id = "raisenow_select_widget",
 *   label = @Translation("RaiseNow Widget Select"),
 *   field_types = {"raisenow_widget_item"},
 * )
 */
class RaiseNowWidgetSelectWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $entityTypes = RaiseNowConfigEntity::getEntityTypes();
    $entityTypeOptions = [
      '' => $this->t('- None -'),
    ];
    foreach($entityTypes as $entityTypeKey => $entityType) {
      $entityTypeOptions[$entityTypeKey] = $entityType->getLabel();
    }

    $entity_type = $items[$delta]->get('entity_type')->getValue();
    if (empty($entityTypeOptions[$entity_type])) {
      $entity_type = '';
    }
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element) && !empty($triggering_element['#ajax']['wrapper']) && $triggering_element['#ajax']['wrapper'] == 'entity_id_wrapper-' . $delta) {
      $parents = $triggering_element['#parents'];
      //array_pop($parents);
      $entity_type = NestedArray::getValue($form_state->getUserInput(), $parents);
    }

    $element['#type'] = 'fieldset';

    $element['entity_type'] = [
      '#title' => $this->t('Entity type'),
      '#type' => 'select',
      '#options' => $entityTypeOptions,
      '#default_value' => isset($entity_type) ? $entity_type : '',
      '#ajax' => [
        'callback' => array($this, 'entityTypeChangedAjax'),
        'event' => 'change',
        'wrapper' => 'entity_id_wrapper-' . $delta,
        'progress' => array(
          'type' => 'throbber',
        ),
      ],
    ];

    if (empty($entity_type)) {
      $element['entity_id'] = [
        '#title' => $this->t('Entity ID'),
        '#markup' => $this->t('Please select an entity type'),
        '#prefix' => '<div id="entity_id_wrapper-' . $delta . '">',
        '#suffix' => '</div>',
      ];
    }
    else {
      $entities = RaiseNowConfigEntity::getEntities($entity_type);
      $entityOptions = [
        '' => $this->t('- None -'),
      ];
      foreach ($entities[$entity_type] as $entityKey => $entity) {
        $entityOptions[$entityKey] = $entity->label();
      }

      $entity_id = $items[$delta]->get('entity_id')->getValue();
      $element['entity_id'] = [
        '#title' => $this->t('Entity ID'),
        '#type' => 'select',
        '#options' => $entityOptions,
        '#default_value' => isset($entity_id) ? $entity_id : '',
        '#prefix' => '<div id="entity_id_wrapper-' . $delta . '">',
        '#suffix' => '</div>',
      ];
    }

    return $element;
  }

  public function entityTypeChangedAjax(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $wrapper_id = $triggering_element['#ajax']['wrapper'];

    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    $parents[] = 'entity_id';

    $element = NestedArray::getValue($form, $parents);

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("#$wrapper_id", $element));
    return $response;
  }

}
