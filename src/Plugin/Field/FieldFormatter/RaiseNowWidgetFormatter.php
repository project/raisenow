<?php

namespace Drupal\raisenow\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'RaiseNow widget' formatter.
 *
 * @FieldFormatter(
 *   id = "raisenow_widget_formatter",
 *   label = @Translation("RaiseNow widget"),
 *   field_types = {
 *     "raisenow_widget_item"
 *   }
 * )
 */
class RaiseNowWidgetFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#title' => $this->t('test'),
        '#type' => 'raisenow_widget',
        '#entity_type' => $item->get('entity_type')->getValue(),
        '#entity_id' => $item->get('entity_id')->getValue(),
      ];
    }

    return $element;
  }

}
