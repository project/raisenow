<?php

/**
 * @file
 * Contains \Drupal\token_filter\Plugin\Filter\TokenFilter.
 */

namespace Drupal\raisenow\Plugin\Filter;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\raisenow\RaiseNowWebApiClient;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a filter that replaces global and entity tokens with their values.
 *
 * @Filter(
 *   id = "raisenow_filter",
 *   title = @Translation("RaiseNow pseudo tokens"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = { }
 * )
 */
class RaiseNowFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $response = RaiseNowWebApiClient::getTransactionFromCurrentPage();
    \Drupal::moduleHandler()->alter('raise_now_filter', $response);
    if (empty($response)) {
      return new FilterProcessResult($text);
    } else {
      $tokens = &drupal_static('raisenow_payment_tokens');
      if (empty($tokens)) {
        $tokens = self::getTokens($response);
      }
      foreach($tokens as $token) {
        if (is_array($token['key']) || is_array($token['value']) || is_array($text)) {
          // do nothing
        }
        else {
          $text = str_replace($token['key'], $token['value'], $text);
        }
      }
      return new FilterProcessResult($text);
    }
  }

  /**
   * @param array|NULL $raisenow_params
   *
   * @return array
   *
   * $raiserow_params = array (
      'transaction_id' => 'c2fn150okac2567',
      'created' => '2017-08-22 08:16:07',
      'timestamp' => '1503382567',
      'amount' => 500,
      'currency' => 'chf',
      'payment_method' => 'AMX',
      'payment_provider' => 'datatrans',
      'css_url' => '/webresourceproxy/load/?uri=https%253A%252F%252Fyour.domain.com%252Fbundles%252Fepayment%252Fcss%252Fdemo.css',
      'further_rnw_interaction' => 'enabled',
      'user_ip' => '212.25.29.110',
      'user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4',
      'http_referer' => 'https://your.domain.com/epayment/demo/single/wait.html',
      'mobile_mode' => 'false',
      'error_url' => 'https://your.domain.com/epayment/demo/return/error',
      'success_url' => 'https://your.domain.com/epayment/demo/return/success',
      'cancel_url' => 'https://your.domain.com/epayment/demo/return/cancel',
      'test_mode' => 'test',
      'language' => 'de',
      'return_parameters' => 'true',
      'datatrans_sign_key' => 'asd',
      'datatrans_merchant_id' => '1100001363',
      'internal_return_executions' => '1',
      'datatrans_upptransaction_id' => '17082208164526614',
      'datatrans_status' => 'success',
      'datatrans_upp_msg_type' => 'web',
      'datatrans_refno' => 'c2fn150okac2567',
      'datatrans_amount' => '500',
      'datatrans_currency' => 'CHF',
      'datatrans_pmethod' => 'AMX',
      'datatrans_reqtype' => 'CAA',
      'datatrans_alias_cc' => 'null',
      'datatrans_masked_cc' => 'null',
      'bankiban' => 'null',
      'bankbic' => 'null',
      'expm' => '12',
      'expy' => '18',
      'datatrans_sign2' => 'asdasd',
      'datatrans_authorization_code' => '645706156',
      'datatrans_response_message' => 'Authorized',
      'datatrans_response_code' => '01',
      'datatrans_acq_authorization_code' => '081645',
      'card_holder_name' => 'Carla',
      'used_redirect_url' => 'https://your.domain.com/epayment/demo/return/success',
      'stored_customer_salutation' => '',
      'stored_customer_firstname' => '',
      'stored_customer_lastname' => '',
      'stored_customer_street' => '',
      'stored_customer_street2' => '',
      'stored_customer_zip_code' => '',
      'stored_customer_city' => '',
      'stored_customer_email' => '',
      'stored_emailpermission' => 'true',
      'stored_campaign_id' => '0',
      'stored_extra' => 'xxxx',
      'status' =>
        array (
          0 =>
          array (
          'created' => '2017-08-22 08:16:07',
          'timestamp' => '1503382567',
          'statusName' => 'new',
          ),
        ),
      );
   */
  public static function getTokens(array $raisenow_params = NULL) {

    $exception = '';
    try {
      $tokens = [
        [
          'key' => '[transaction_id]',
          'description' => t('The RaiseNow transaction ID'),
          'value' => isset($raisenow_params['epp_transaction_id']) ? $raisenow_params['epp_transaction_id'] : '',
        ],
        [
          'key' => '[created]',
          'description' => t('The created date of the transaction'),
          'value' => isset($raisenow_params['created']) ? $raisenow_params['created'] : '',
        ],
        [
          'key' => '[raw_amount]',
          'description' => t('A raw donation amount, e.g. 50'),
          'value' => isset($raisenow_params['amount']) ? $raisenow_params['amount'] : '',
        ],
        [
          'key' => '[currency_code]',
          'description' => t('The donation currency code, e.g usd'),
          'value' => isset($raisenow_params['currency']) ? $raisenow_params['currency'] : '',
        ],
        [
          'key' => '[donation_amount]',
          'description' => t('A localised and formatted donation amount, e.g. $50'),
          'value' => '', //isset($raisenow_params) ? $raisenow_params[''] : '',
        ],
        [
          'key' => '[payment_method]',
          'description' => t('The payment method'),
          'value' => isset($raisenow_params['payment_method']) ? $raisenow_params['payment_method'] : '',
        ],
        [
          'key' => '[payment_provider]',
          'description' => t('The payment provider'),
          'value' => isset($raisenow_params['payment_provider']) ? $raisenow_params['payment_provider'] : '',
        ],
        [
          'key' => '[salutation]',
          'description' => t('The donor\'s salutation'),
          'value' => isset($raisenow_params['stored_customer_salutation'] ) ? $raisenow_params['stored_customer_salutation'] : '',
        ],
        [
          'key' => '[first_name]',
          'description' => t('The donor\'s first name'),
          'value' => isset($raisenow_params['stored_customer_firstname']) ? $raisenow_params['stored_customer_firstname'] : '',
        ],
        [
          'key' => '[last_name]',
          'description' => t('The donor\'s last name'),
          'value' => isset($raisenow_params['stored_customer_lastname']) ? $raisenow_params['stored_customer_lastname'] : '',
        ],
        [
          'key' => '[street]',
          'description' => t('The donor\'s street 1'),
          'value' => isset($raisenow_params['stored_customer_street']) ? $raisenow_params['stored_customer_street'] : '',
        ],
        [
          'key' => '[street2]',
          'description' => t('The donor\'s street 2'),
          'value' => isset($raisenow_params['stored_customer_street2'] ) ? $raisenow_params['stored_customer_street2'] : '',
        ],
        [
          'key' => '[zip_code]',
          'description' => t('The donor\'s zip code'),
          'value' => isset($raisenow_params['stored_customer_zip_code']) ? $raisenow_params['stored_customer_zip_code'] : '',
        ],
        [
          'key' => '[city]',
          'description' => t('The donor\'s city'),
          'value' => isset($raisenow_params['stored_customer_city']) ? $raisenow_params['stored_customer_city'] : '',
        ],
        [
          'key' => '[email]',
          'description' => t('The donor\'s email'),
          'value' => isset($raisenow_params['stored_customer_email']) ? $raisenow_params['stored_customer_email'] : '',
        ],
        [
          'key' => '[card_id]',
          'description' => t('For example, the last four digits of the card number, if the payment provider supports this'),
          'value' => isset($raisenow_params['stripe_source_last4']) ? $raisenow_params['stripe_source_last4'] : '',
        ],
        [
          'key' => '[exception_message]',
          'description' => t('The error message from attempting to process the donation, if there is one'),
          'value' => isset($raisenow_params['exception']) ? $raisenow_params['exception'] : '',
        ],
      ];
    }
    catch (\Throwable $ex) {
      $tokens = [
        ['key' => '[exception_message]',
          'description' => t('The error message from attempting to process the donation, if there is one'),
          'value' => $ex->getMessage(),
        ],
      ];
    }

    return $tokens;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = $this->t('Thank you tokens are replaced with their values.');

    $header = [
      $this->t('You type'),
      $this->t('You get')
    ];

    $tokens = self::getTokens();

    $rows = [];
    foreach ($tokens as $token) {
      $rows[] = [
        [
          'data' => [
            '#prefix' => '<code>',
            '#plain_text' => $token['key'],
            '#suffix' => '</code>'
          ],
          'class' => [
            'token_key'
          ],
        ],
        [
          'data' => $token['description'],
          'class' => ['description'],
        ],
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
    $output .= \Drupal::service('renderer')->render($table);
    return $output;
  }
}
