<?php

namespace Drupal\raisenow\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Product Type' condition.
 *
 * @Condition(
 *   id = "rasienow_entity",
 *   label = @Translation("RaiseNow Entity"),
 * )
 */
class RaiseNowEntityCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Creates a new QuickPurchaseProductTypeCondition instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['raisenow_entity'] = [
      '#title' => $this->t('The page is a RaiseNow entity'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['raisenow_entity'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['raisenow_entity'] = $form_state->getValue('raisenow_entity');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if (count($this->configuration['raisenow_entity']) == 1) {
      return $this->t('The page is a RaiseNow entity');
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $route_name = \Drupal::routeMatch()->getRouteName();
    return $this->configuration['raisenow_entity'] && $route_name == 'entity.raisenow_tamaro_widget.canonical';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['raisenow_entity' => 0] + parent::defaultConfiguration();
  }

}
