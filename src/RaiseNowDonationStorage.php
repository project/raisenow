<?php

namespace Drupal\raisenow;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\raisenow\Entity\RaiseNowDonationInterface;

/**
 * Defines the storage handler class for RaiseNow donation entities.
 *
 * This extends the base storage class, adding required special handling for
 * RaiseNow donation entities.
 *
 * @ingroup raisenow
 */
class RaiseNowDonationStorage extends SqlContentEntityStorage implements RaiseNowDonationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(RaiseNowDonationInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {raise_now_donation_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {raise_now_donation_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
