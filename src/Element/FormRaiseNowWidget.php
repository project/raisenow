<?php

namespace Drupal\raisenow\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\raisenow\RaiseNowConfigEntity;
use Drupal\raisenow\RaiseNowConfigEntityInterface;

/**
 * Provides a render element to display an entity.
 *
 * Properties:
 * - #entity_type: The entity type.
 * - #entity_id: The entity ID.
 * - #view_mode: The view mode that should be used to render the entity.
 * - #langcode: For which language the entity should be rendered.
 *
 * Usage Example:
 * @code
 * $build['form_raisenow_widget'] = [
 *   '#type' => 'form_raisenow_widget',
 *   '#entity_type' => 'tamaro_widget',
 *   '#entity_id' => 'example',
 *   '#langcode' => 'en',
 * ];
 * @endcode
 *
 * @RenderElement("form_raisenow_widget")
 */
class FormRaiseNowWidget extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#default_value' => NULL,
      '#element_validate' => [
        [$class, 'validateRaiseNowWidget'],
      ],
      '#process' => [
        [$class, 'processRaiseNowWidget'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Processes the address_country form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #available_countries is malformed.
   */
  public static function processRaiseNowWidget(&$element, FormStateInterface $form_state, &$complete_form) {
    // check we have some properies
    if (empty($element['#entity_type']) || empty($element['#entity_id'])) {
      return $element;
    }
    $element['raisenow_widget'] = [
      '#type' => 'raisenow_widget',
      '#entity_type' => $element['#entity_type'],
      '#entity_id' => $element['#entity_id'],
    ];
    // get the webform submission and webform object
    /** @var \Drupal\webform\WebformSubmissionForm $submission */
    $submissionForm = $form_state->getFormObject();
    if (empty($submissionForm)) {
      return $element;
    }
    $webform = $submissionForm->getWebform();
    if (empty($webform)) {
      return $element;
    }
    // get the configured mappings
    $config = $webform->getThirdPartySetting('raisenow_mappings', 'element');
    if (empty($config)) {
      return $element;
    }
    $mappings = $config['raisenow_mapping'];
    if (empty($mappings)) {
      return $element;
    }
    /** @var \Drupal\webform\WebformSubmissionInterface $submission */
    $submission = $submissionForm->getEntity();
    if (empty($submission)) {
      return $element;
    }
    $submission_data = $submission->getRawData();
    if (empty($submission_data)) {
      return $element;
    }

    $raisenow_options = [];
    $raiseNowMappings = RaiseNowConfigEntity::getMappings();
    if (!empty($raiseNowMappings[$element['#entity_type']])) {
      static::applyMappings($submission_data, $mappings, $raiseNowMappings[$element['#entity_type']], $raisenow_options);
      if (!empty($raisenow_options)) {
        $element['raisenow_widget']['#raisenow_options'] = $raisenow_options;
      }
    }

    \Drupal::moduleHandler()->alter('raisenow_widget_form_element', $element);

    return $element;
  }

  /**
   * @param $submission_data
   * @param $mappings
   * @param $options
   */
  public static function applyMappings($submission_data, $mappings, $raisenow_mappings, &$options) {
    foreach($submission_data as $submission_source => $submission_value) {
      if (is_array($submission_value)) {
        if (isset($mappings[$submission_source])) {
          static::applyMappings($submission_data[$submission_source], $mappings[$submission_source], $raisenow_mappings,  $options);
        }
      }
      else {
        if (isset($mappings[$submission_source]['mapping']) && $submission_value !== '') {
          $raisenow_id = $mappings[$submission_source]['mapping'];
          $options['paymentFormPrefill'][$raisenow_id] = $submission_value;
          if (!empty($raisenow_mappings[$raisenow_id]['elementID']) && !empty($mappings[$submission_source]['hide'])) {
            $options[$raisenow_mappings[$raisenow_id]['elementID']] = false;
          }
        }
      }
    }
  }

  /**
   * Processes the address_country form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #available_countries is malformed.
   */
  public static function validateRaiseNowWidget(&$element, FormStateInterface $form_state, &$complete_form) {
    $a=1;
  }
}


