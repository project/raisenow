<?php

namespace Drupal\raisenow\Element;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\raisenow\RaiseNowConfigEntityInterface;

/**
 * Provides a donation_amounts form element.
 *
 * Usage example:
 * @code
 * $element['raisenow_widget'] = [
 *   '#type' => 'raisenow_widget',
 *   '#entity_type' => 'tamaro_widget',
 *   '#entity_id' => 'example',
 * ];
 * @endcode
 *
 * @RenderElement("raisenow_widget_delete")
 */
class RaiseNowWidgetDelete extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#donation_amounts' => [],
      '#entity_type' => NULL,
      '#entity_id' => NULL,
      '#process' => [
        [$class, 'processTamaroWidget'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
    ];
  }

  /**
   * Processes the address_country form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #available_countries is malformed.
   */
  public static function processTamaroWidget(&$element, FormStateInterface $form_state, &$complete_form) {
    // check we have some properies
    if (empty($element['#entity_type']) || empty($element['#widget'])) {
      return $element;
    }

    // try to get the entity from storage
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $entityTypeStorage = $entityTypeManager->getStorage($element['#entity_type']);
    if (empty($entityTypeStorage)) {
      return $element;
    }
    $widgetEntity = $entityTypeStorage->load($element['#widget']);
    // quit if there is no matching entity or it's not a RaiseNow entity
    if (empty($widgetEntity) || !$widgetEntity instanceof RaiseNowConfigEntityInterface) {
      return $element;
    }

    $element['#widget_entity'] = $widgetEntity;

    if (empty($widget)) {
      return $element;
    }

    $config = $widget->raiseNowConfig();


    \Drupal::moduleHandler()->alter('donation_amounts_element', $element);

    return $element;
  }
}
