<?php

namespace Drupal\raisenow\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\raisenow\RaiseNowConfigEntityInterface;

/**
 * Provides a render element to display an entity.
 *
 * Properties:
 * - #entity_type: The entity type.
 * - #entity_id: The entity ID.
 * - #view_mode: The view mode that should be used to render the entity.
 * - #langcode: For which language the entity should be rendered.
 *
 * Usage Example:
 * @code
 * $build['raisenow_widget'] = [
 *   '#type' => 'raisenow_widget',
 *   '#entity_type' => 'tamaro_widget',
 *   '#entity_id' => 'example',
 *   '#raisenow_options' => [],
 *   '#langcode' => 'en',
 * ];
 * @endcode
 *
 * @RenderElement("raisenow_widget")
 */
class RaiseNowWidget extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#donation_amounts' => [],
      '#entity_type' => NULL,
      '#entity_id' => NULL,
      '#raisenow_options' => [],
      '#pre_render' => [
        [get_class($this), 'preRenderRaiseNowWidget'],
      ],
      '#view_mode' => 'full',
      '#langcode' => NULL,
    ];
  }

  /**
   * Entity element pre render callback.
   *
   * @param array $element
   *   An associative array containing the properties of the entity element.
   *
   * @return array
   *   The modified element.
   */
  public static function preRenderRaiseNowWidget(array $element) {

    // check we have some properies
    if (empty($element['#entity_type']) || empty($element['#entity_id'])) {
      return $element;
    }

    // try to get the entity from storage
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $entityTypeStorage = $entityTypeManager->getStorage($element['#entity_type']);
    if (empty($entityTypeStorage)) {
      return $element;
    }
    /** @var RaiseNowConfigEntityInterface $widgetEntity */
    $widgetEntity = $entityTypeStorage->load($element['#entity_id']);
    $widgetEntity->setRaiseNowOptions(isset($element['#raisenow_options']) ? $element['#raisenow_options'] : []);
    // quit if there is no matching entity or it's not a RaiseNow entity
    if (empty($widgetEntity) || !$widgetEntity instanceof RaiseNowConfigEntityInterface) {
      return $element;
    }

    $element['#widget_entity'] = $widgetEntity;
    $element = $widgetEntity->getWidgetRenderArray($element);

    \Drupal::moduleHandler()->alter('raisenow_widget', $element);

    return $element;
  }

}
