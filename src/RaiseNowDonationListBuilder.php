<?php

namespace Drupal\raisenow;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of RaiseNow donation entities.
 *
 * @ingroup raisenow
 */
class RaiseNowDonationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('RaiseNow donation ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\raisenow\Entity\RaiseNowDonation $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.raise_now_donation.edit_form',
      ['raise_now_donation' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
